<?php

  namespace Tests\Feature;

  use App\Product;
  use Illuminate\Http\UploadedFile;
  use Illuminate\Support\Facades\DB;
  use Illuminate\Support\Facades\Storage;
  use Tests\TestCase;


  class ProductCRUDTest extends TestCase
  {

    protected function setUp()
    {
      parent::setUp();
      DB::Table('products')->truncate();
    }

    /**
     * @test
     * @return void
     */
    public function can_get_into_products_index_page()
    {
      $this->get('/')->assertStatus(200)->assertSeeText('Add New Product');
    }


    /**
     *
     * @test
     */

    public function can_upload_a_product_picture()
    {
      Storage::fake('public/products');
      $file = $this->getFile();

      $response = $this->json('POST', 'products', [
        'picture' => $file,
        'name' => 'shoes',
        'description' => 'this is the description for the shoes in test',
        'price' => 12.99
      ]);
      $response->assertStatus(302)->assertSessionHas('status', 'Product created successfully');
      Storage::disk('public')->assertExists('products/' . $file->getClientOriginalName());
    }

    /**
     *
     * @test
     */
    public function can_show_create_a_product_page()
    {
      $this->get(route('products.create'))
        ->assertStatus(200)
        ->assertSeeText('Create a new product');
    }


    /**
     *
     * @test
     */
    public function can_edit_a_product()
    {
      $product = factory(Product::class)->create();

      $this->get(route('products.edit', $product->id))
        ->assertStatus(200)
        ->assertSeeText('Editing - ' . $product->name)
        ->assertSeeText($product->name);
    }


    /**
     *
     * @test
     */
    public function can_update_a_product()
    {
      factory(Product::class)->create();
      $product = Product::get()->first();

      $file = $this->getFile();
      $data = [
        'name' => 'new random name',
        'picture' => $file,
        'price' => rand(10, 50),
        'description' => 'test',
        '_token' => Session()->token()
      ];

      $this->json('PATCH', route('products.update', $product->id), $data)
        ->assertStatus(302)
        ->assertSessionHas('status', 'Product updated successfully');

      $updatedProduct = Product::find($product->id);

      $this->assertEquals($data['name'], $updatedProduct->name);
      $this->assertEquals('/products/' . $file->getClientOriginalName(), $updatedProduct->picture);
      $this->assertEquals($data['price'], $updatedProduct->price);


    }


    /**
     * @test
     */
    public function can_create_a_new_product()
    {

      $product = factory(Product::class)->create([
        'name' => 'ali',
        'price' => 99,
        'picture' => $this->getFile(),
        'description' => 'this is the description',
      ]);

      $saveItem = Product::find($product->id);
      $this->assertNotNull($saveItem);
    }


    /**
     * @test
     */
    public function can_delete_a_product()
    {
      factory(Product::class)->create();
      $product = Product::get()->first();
      $response = $this->json('DELETE', route('products.destroy', $product->id))
        ->assertStatus(302);
      $response->assertSessionHas('status', 'Product deleted successfully');
      $this->assertNull(Product::find($product->id));
    }

    private function getFile($imageName = 'TestImage.jpg')
    {
      return UploadedFile::fake()->image($imageName);
    }

  }
