Framework
==========
I decided to use Laravel because it the framework which i am most comfortable in terms of my familiarity.




Simple Product Management System
================================
Design and implement a simple product management solution using PHP with CRUD functionality

Getting Started
==============
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.
 
 
 System Requirements
 ===============
 
 I have developed this on windows 10 but so you may have to think with the configurations to a little.
        
Prerequisites
=============
1) Composer installed and add to your environment path

2) Database server, e.g XAMPP
 
 
SET UP 
==================
 __Its assumed you have installed and set up Composer and XAMPP, or LAMP or WAMP installed on your machine and database server started, if not, install one of those.__ 
   
 1) Start your database server and create the following databases.
 
    * brighte_dev
    * brighte_test
   		
2) Clone project

    `git clone git@bitbucket.org:Ali-251/product-management-system.git`
    
    `cd path/to/cloned/project/`
    
3) Open the project in your favourite editor and  set up your .env file configurations.
    There is `.env.test` file which you can just rename to `.env`, otherwise create `.env` file and put the your database configurations similiar to `.env.test` file. Make sure your set the password, username and DB_DATABASE and DB_DATABASE_TEST match the schemas you created in step 1
    
    `
    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=brighte_dev
    DB_USERNAME=root
    DB_PASSWORD=
    DB_DATABASE_TEST=brighte_test`
   

4) Run the following commands in your terminal

    ```
    composer install
    php artisan key:generate 
    php artisan storage:link`
    
    
5) #### Migrations

    Run the following commands in your terminal: <br>
    
    `php artisan migrate:fresh`

    `php artisan migrate:fresh --database=mysql_test`
	 
6) Start The application in your terminal 
 
	 `php artisan serve`
	 
 7) Done - to test open a browser and go to `http://localhost:8000/products`
 
 
 
Running Tests
========================
 
 
In your terminal window `vendor/bin/phpunit`


==========================================
##### Time Taken ####

I spend around 15 hours to complete this task. 
I had some challenges writing the unit tests but the actual CRUD took me around 3 hours.


##### Compromises/shortcuts

I took significant short cuts on the html and css side of things due to time consideration.

In regards to design choices, I decided to use Repository and Services classes in order to eliminate any logic from the controller.

It would have been much simple to leave everything in the controller but I found the way resources are stored do change over time
so using the controller to store a product would be less maintainable.

You could argue, do you need the repository class and interface? Which is a valid point but my think was all Model could extend that.