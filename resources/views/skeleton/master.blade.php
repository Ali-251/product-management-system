<html>
    <head>
        <title>Product Management System</title>
        @include("skeleton.header")
    </head>
    <body>
        @yield('content')
    </body>
</html>