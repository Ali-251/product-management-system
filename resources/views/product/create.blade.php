@extends('skeleton.master')

@section('content')

    <div class="container">

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="panel panel-default">
            <div class="panel-heading"><h1>Create a new product</h1></div>
            <div class="panel-body">
                <form action="{{route('products.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label> Name</label>
                                <input class="form-control" type="text" name="name" placeholder="Enter Product Name"
                                       required value="{{old('name')}}">
                            </div>

                            {{--Descrition--}}

                            <div class="form-group">
                                <label></b>Description</label><br>

                                <textarea class="form-control" name="description"
                                          required>{{ old('description') }}</textarea>
                            </div>

                            {{--price --}}
                            <div class="form-group">
                                <label> Price</label>
                                <input class="form-control" name="price" placeholder="Enter Product Name" type="number"
                                       step="0.01" required value="{{old('price')}}">
                            </div>

                            {{--image--}}
                            <div class="form-group">
                                <label> Upload Image * </label>
                                <input class="form-control" name="picture" placeholder="Enter Product Name" type="file"
                                       required value="{{old('picture')}}">
                            </div>


                            <div class="form-group">
                                <ul>
                                    @foreach($errors->all()  as $message)
                                        <li class="alert alert-danger">{{$message}}</li>
                                    @endforeach
                                </ul>

                            </div>
                            <button type="submit" class="btn btn-success" STYLE="width: 100%">Create</button>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>





@endsection