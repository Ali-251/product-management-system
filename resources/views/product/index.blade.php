@extends('skeleton.master')

@section('content')

    <div class="container" style="margin-top: 100px">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <div class="text-center">
            <a class="btn btn-primary" href="{{route('products.create')}}"> Add New Product</a>
        </div>
        @if($products->isEmpty())
            <p>No products found</p>
        @else

            <h1>Available products</h1>
            <div class="float-right">
                <p>Order By </p>
                <a href="/products/?sort=name&direction={{ request('direction') === 'asc'? 'desc': 'asc'}}">Name</a>
                <a href="/products/?sort=price&direction={{ request('direction') === 'asc'? 'desc': 'asc'}}">Price</a>
            </div>
            <table class="table table-bordered">
                <thead class="table thead-dark">
                <tr>
                    <th>Picture</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Description</th>
                    <th>Actions</th> {{--edit--}}

                </tr>
                </thead>
                <tbody>
                @foreach($products as $product)

                    <tr class="form-group">
                        <div class="form-group" style="margin: 20px;">
                            <td><a href="{{ route('products.show', $product->id) }}"> <img
                                            src='{{ asset("storage/$product->picture") }}' style="width: 200px; height: auto;"> </a></td>
                            <td><a href="{{ route('products.show', $product->id) }}"> {{ $product->name }} </a></td>
                            <td> {{ $product->price }}</td>
                            <td>  {{ $product->description }} </td>
                            <td>
                                <p><a class="alert alert-primary" href="{{route('products.edit', $product->id)}}">
                                        Edit</a></p>
                                <p>
                                <form action="{{route('products.destroy',$product->id) }}" method="POST"
                                      onsubmit="return confirm('You sure you want to delete?');">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE"/>
                                    <button type="submit" class="btn btn btn-danger">Delete</button>
                                </form>
                                </p>
                            </td>

                        </div>
                    </tr>
                @endforeach
                </tbody>

            </table>

            {{ $products->links() }}
        @endif
    </div>

@endsection