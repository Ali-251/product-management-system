@extends('skeleton.master')

@section('content')
    <div class="container" style="margin-top: 100px">

        <span class="text-center">
            <a href="{{ route('products.index') }}" class="btn btn-primary">Go Back</a>
        </span>
        <div class="panel panel-default">
            <div class="panel-heading"><h3>Editing - {{$product->name}} </h3></div>
            <div class="panel-body">

                <form action="{{ route('products.update', $product->id) }}" method="POST" enctype="multipart/form-data">
                    @method('PATCH')
                    @csrf
                    <div class="row">
                        <div class="col">
                            <div class="form-group">

                                <input type="hidden" name="id" value="{{$product->id}}">
                                <label> Name</label>
                                <input class="form-control" type="text" name="name" placeholder="Enter Product Name"
                                       required value="{{$product->name}}">
                            </div>

                            {{--Descrition--}}

                            <div class="form-group">
                                <label></b>Description</label><br>

                                <textarea class="form-control" name="description"
                                          required>{{ $product->description }}</textarea>
                            </div>

                            {{--price --}}
                            <div class="form-group">
                                <label> Price</label>
                                <input class="form-control" name="price" placeholder="Enter Product Name" type="number"
                                       step="0.01" required value="{{$product->price}}">
                            </div>

                            {{--image--}}
                            <div class="form-group">
                                <img src="{{asset("storage/$product->picture")}}">
                                <p>Upload a different picture</p>
                                <input class="form-control" name="picture" placeholder="Enter Product Name" type="file"
                                       value="{{$product->picture}}">
                                <input type="hidden" name="image" value="{{$product->picture}}">
                            </div>

                            @if($errors->IsNotEmpty())
                                <div class="form-group">
                                    <ul>
                                        @foreach($errors->all()  as $message)
                                            <li class="alert alert-danger">{{$message}}</li>
                                        @endforeach
                                    </ul>

                                </div>
                            @endif
                            <button type="submit" class="btn btn-success" STYLE="width: 100%">Save Changes</button>

                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
@endsection