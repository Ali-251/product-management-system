@extends('skeleton.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <h3>{{ $product->name }}</h3>
                <img src='{{asset("storage/$product->picture") }}'>
                <p> ${{ $product->price }}</p>
                <p>{{ $product->description }}</p>
                <span class="text-center">
                    <a href="{{ route('products.index') }}" class="btn btn-primary">Go Back</a>
                </span>
            </div>
        </div>
    </div>
@endsection