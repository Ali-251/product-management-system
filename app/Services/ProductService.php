<?php
  /**
   * Created by PhpStorm.
   * User: Bozo
   * Date: 9/10/2018
   * Time: 8:01 PM
   */

  namespace App\Services;


  use App\Http\Requests\ProductStoreRequest;
  use App\Product;
  use App\Repositories\ProductRepository;
  use Illuminate\Http\Request;

  class ProductService
  {
    protected $productRepository;

    /**
     * Product constructor.
     * @param ProductService $productModel
     */
    public function __construct(Product $productModel)
    {
      $this->productRepository = new ProductRepository($productModel);
    }

    /**
     * @param Request $request
     * @param array $filters
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getAll(Request $request, array $sortBy = [], $perPage = 10)
    {
      if (in_array($request->sort, $sortBy)) {
        return $this->productRepository->query()->orderBy(
          $request->sort,
          $request->get('direction', 'asc')
        )->paginate($perPage)->appends($request->query());

      } else {
        return $this->productRepository->query()->paginate($perPage)->appends($request->query());
      }

    }

    public function create(ProductStoreRequest $request)
    {
      return $this->productRepository->create(array_merge($request->all(),
        array('picture' => $this->storeProductImage())));
    }

    public function update(ProductStoreRequest $request, $id)
    {
      return $this->productRepository->update(
        array_merge($request->all(), ['picture' => $this->getProductImagePath()]),
        $id
      );
    }

    public function delete($id)
    {
      return $this->productRepository->delete($id);
    }

    public function show($id)
    {
      return $this->productRepository->show($id);
    }

    /**
     * @returns imagePath - where the NEW file upload is stored
     */
    private function storeProductImage()
    {
      $filePath = sprintf("/%s/%s", 'products', \request()->file('picture')->getClientOriginalName());
      request()->file('picture')->storeAs('public/products', \request()->file('picture')->getClientOriginalName(),
        'local');
      return $filePath;
    }

    /**
     * @returns imagePath - the path of the stored product image
     */
    private function getProductImagePath()
    {
      if (\request()->has('picture')) {
        return $this->storeProductImage();
      }
      return \request('image');
    }
  }