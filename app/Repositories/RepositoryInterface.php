<?php
  /**
   * Created by PhpStorm.
   * User: Bozo
   * Date: 9/10/2018
   * Time: 7:42 PM
   */

  namespace App\Repositories;


  Interface RepositoryInterface
  {
    public function all();

    public function create(array $data);

    public function update(array $data, $id);

    public function delete($id);

    public function show($id);

    public function paginate($perPage = 25);
  }