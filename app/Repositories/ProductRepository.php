<?php
  /**
   * Created by PhpStorm.
   * User: Bozo
   * Date: 9/10/2018
   * Time: 7:42 PM
   */

  namespace App\Repositories;

  use Illuminate\Database\Eloquent\Model;

  class ProductRepository implements RepositoryInterface
  {
    protected $model;

    public function __construct(Model $model)
    {
      $this->model = $model;
    }

    public function query()
    {
      return $this->model::query();
    }

    public function all()
    {
      return $this->model->all();
    }

    public function create(array $data)
    {
      return $this->model->create($data);
    }

    public function update(array $data, $id)
    {
      $record = $this->model->findOrFail($id);
      return $record->update($data);
    }

    public function show($id)
    {
      return $this->model->findOrFail($id);
    }

    public function delete($id)
    {
      $this->model->destroy($id);
    }

    public function paginate($perPage = 25)
    {
      return $this->model->paginate($perPage);
    }
  }