<?php

  namespace App\Http\Requests;

  use Illuminate\Foundation\Http\FormRequest;

  class ProductStoreRequest extends FormRequest
  {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
      return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      $rules = [
        'name' => 'required|min:3|max:255',
        'price' => 'required|numeric',
        'description' => 'required|min:3',
        'picture' => 'required|mimes:jpg,jpeg,gif,png'
      ];

      /**  ================edit============================================
       * its not must to upload a new image as we can still use the previously uploaded image
       */
      if ($this->get('id')) {
        unset($rules['picture']);
        $rules['image'] = 'required'; /*hidden input that contains the currently stored image path. This will always be set on edit page*/
      }

      return $rules;
    }
  }
