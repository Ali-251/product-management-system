<?php

  namespace App\Http\Controllers;

  use App\Http\Requests\ProductStoreRequest;
  use App\Product;
  use App\Services\ProductService as ProductService;
  use Illuminate\Http\Request;

  class ProductController extends Controller
  {
    protected $productService;

    public function __construct(Product $productModel)
    {
      $this->productService = new ProductService($productModel);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $sortable = array('name', 'price');
      $products = $this->productService->getAll($request, $sortable);
      return view('product.index', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductStoreRequest $request
     * @return void
     */
    public function store(ProductStoreRequest $request)
    {
      $this->productService->create($request);
      return redirect()->route('products.index')->with(['status' => 'Product created successfully']);

    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return Product
     */
    public function show($id)
    {
      return view('product.show', ['product' => $this->productService->show($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      return view('product.edit', ['product' => $this->productService->show($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductStoreRequest $request
     * @param Request
     * @return void
     */
    public function update(ProductStoreRequest $request, $id)
    {
      $this->productService->update($request, $id);
      return redirect()->route('products.index')->with(['status' => 'Product updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return void
     */
    public function destroy($id)
    {
      $this->productService->delete($id);
      return redirect()->route('products.index')->with(['status' => 'Product deleted successfully']);;
    }

  }
