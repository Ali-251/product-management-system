<?php

  use Faker\Generator as Faker;

  $factory->define(App\Product::class, function (Faker $faker) {
    return [
      'name' => $faker->name,
      'description' => $faker->paragraph,
      'price' => $faker->numberBetween(5, 1000),
      'picture' => 'products/LxLtQIc9IuJUZj9wkAC4tqg7hQU5FsUwGst7BWHO.jpeg',
    ];
  });
