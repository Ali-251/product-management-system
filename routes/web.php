<?php

  Route::get('/', 'ProductController@index');
  Route::resource('/products', 'ProductController');

  Route::post('/avatar', 'ProductController@avatar');